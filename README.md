# Python

[![pipeline status](https://gitlab.com/crazyuploader/Python/badges/master/pipeline.svg)](https://gitlab.com/crazyuploader/Python/commits/master)

Just my Python Programs which can be run online [HERE](https://python.jugalkishore.repl.run).

<b>[`hello.py`](hello.py)</b>
* First Program to print 'Hello World!' & Print an Entered Name.

<b>[`add_2.py`](add_2.py)</b>
* Program to Add two Entered Number(s).

<b>[`subtract_2.py`](subtract_2.py)</b>
* Program to Subtract two Entered Number(s).

<b>[`multiplication_2.py`](multiplication_2.py)</b>
* Program to Multiply two Entered Number(s).

<b>[`division_2.py`](division_2.py)</b>
* Program to Divide two Entered Number(s).

<b>[`even_odd.py`](even_odd.py)</b>
* Program to check if Entered Number is Even or Odd.

<b>[`factorial.py`](factorial.py)</b>
* Program to Calculate Factorial of an Entered Number.

<b>[`greater_2.py`](greater_2.py)</b>
* Program to Display Greater Number among two Entered Number(s).

<b>[`greater_3.py`](greater_3.py)</b>
* Program to Display Greater Number among three Entered Number(s).

<b>[`base_exponent_power.py`](base_exponent_power.py)</b>
* Program to Calculate Power of a Number.

<b>[`reverse.py`](reverse.py)</b>
* Program to Reverse an Entered Number.

<b>[`palindrome.py`](palindrome.py)</b>
* Program to Check if Entered Number is Palindrome or not.

<b>[`average.py`](average.py)</b>
* Program to Get Average of Entered Number(s).

<b>[`table.py`](average.py)</b>
* Program to Show Table of Entered Number.

<b>[`prime.py`](prime.py)</b>
* Program to Check if Entered Number is Prime or not.

<b>[`armstrong.py`](armstrong.py)</b>
* Program to Check Whether or not Entered Number is Armstrong.

<b>[`sum_digits.py`](sum_digits.py)</b>
* Program to Display Sum of its Digit(s).

<b>[`number_swap.py`](number_swap.py)</b>
* Program to Swap Two Entered Variables.
